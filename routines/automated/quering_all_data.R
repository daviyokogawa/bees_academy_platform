library(tidyverse)
library(foreach)
library(doParallel)

source("../funcoes/funcoes.R")

all_courses <-  unique(APITalentMS(requisicao = "courses")$id)
all_branches <- unique(APITalentMS(requisicao = "branches")$id)

user_info  <-  APITalentMS(requisicao = "users") %>%
  select(
    user_id = id, user_login = login,
    main_user_type =  user_type, user_status = status,
    user_points = points, contains("custom_field"))

users_from_courses <- parallel_extract(all_courses, type = "courses", n_cluster = 8)
users_from_branches <- parallel_extract(all_branches, type = "branches", n_cluster = 8)

users_join <- users_from_courses %>%
  left_join(users_from_branches, by = c("user_id", "user_name")) %>%
  left_join(user_info, by = "user_id")

write_rds(users_join, "Arquivos/users_all_courses_branches.rds")
