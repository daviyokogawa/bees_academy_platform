library(tidyverse)
source("funcoes/funcoes.R")

elscxc <- "16"

branches_id <- c(elscxc)


users <- Usuarios()
branches <- APITalentMS(
    requisicao = "branches",
    method = "GET",
)


branch_users <- data.frame(
    id = character(),
    name = character(),
    branch_id = character(),
    branch_name = character(),
    stringsAsFactors = FALSE
)

for (branch in branches_id) {
    branch_data <- APITalentMS(
        requisicao = "branches",
        method = "GET",
        body = list(
            id = branch
        )
    )
    branch_id <- branch_data$id
    branch_name <- branch_data$name
    users_data <- branch_data$users %>%
        mutate(
            branch_id = branch_id,
            branch_name = branch_name
        )
    rm(branch_data, branch_id, branch_name)
    branch_users <- rbind(branch_users, users_data)
}

branch_users <- branch_users %>%
    select(
        user_id = id,
        branch_id,
        branch_name
    ) %>%
    left_join(
        users %>%
        select(user_id = id, login),
        by = "user_id"
    )

completing_courses <- data.frame(
    Usertocourses = branch_users$login,
    course = "Club B (AF_04_ES_006)",
    EnrolledOnDate = "01/09/2023",
    CompletionDate = "27/09/2023",
    Status = "completed",
    Score = "100%"
)

openxlsx::write.xlsx(
    completing_courses,
    file = "~/Desktop/completing_courses.xlsx",
)
