library(tidyverse)

from_to_branch_name_to_code = function(){
  
  from_to_BDR = data.frame(branch_name = c("colbdr",
                                           "colserdanbdr",
                                           "coldislicoresbdr",
                                           "colsociosbdr",
                                           "colmercadobdr",
                                           "bolbdr",
                                           "argbdr",
                                           "mexbdr",
                                           "elsbdr",
                                           "argtercerosbdr",
                                           "dombdr",
                                           "ecubdr",
                                           "honbdr",
                                           "panbdr",
                                           "parbdr",
                                           "perbdr",
                                           "tanbdr",
                                           "ugabdr",
                                           "escanbdr",
                                           "mercasidbdr",
                                           "nestleadnec",
                                           "partnersnestlephbdr",
                                           "nestlebr",
                                           "gloriaco", # comeca aq
                                           "loreal",
                                           "nestlear",
                                           "nestlebr",
                                           "nestleindonesia",# termina aq
                                           "belbdr",
                                           "nedbdr",
                                           'safbdr', 'brabdr', 'gerbdr', "nestlephilippines", 'uknbdr'),
                           
                           code = c("CO","CO","CO","CO","CO","BO","AR","MX",# add o colombia terceiros na propria colombia
                                    "SV","AR","DR",
                                    "EC","HN","PA",
                                    "PY","PE","TZ",
                                    "UG","ESCAN",
                                    "MERCASID", "NESTLEADNEC", "NESTLEPH", "NESTLEBR",
                                    "GLORIACO", "LOREAL", "NESTLEAR", "NESTLEINDONESIA",
                                    "PARTNERSBR",
                                    "BE", "NL", "ZA", "BR", 'GE', "NESTLEPH", "UK"),
                           audiencia_dev = "BDR")
  
  from_to_CXC = data.frame(branch_name = c("percxc",
                                           "mexcxc",
                                           "domcxc",
                                           "colcxc",
                                           "pancxc",
                                           "argcxc",
                                           "parcxc",
                                           "bolcxc",
                                           "bracxc",
                                           "educxc",
                                           "ecucxc",
                                           "elscxc",
                                           "honcxc",
                                           "tancxc",
                                           "ugacxc",
                                           "usacxc",
                                           "escancxc",
                                           "cancxc",
                                           "safcxc",
                                           "gercxc",
                                           "belcxc",
                                           "nedcxc"),
                           
                           code = c("PE","MX","DR",
                                    "CO","PA","AR",
                                    "PY","BO","BR","EC","EC",
                                    "SV","HN","TZ",
                                    "UG","US","ESCAN","CA",
                                    "ZA","DE","BE", "NL"),
                           audiencia_dev = "CXC")
  
  
  from_to_DEL = data.frame(branch_name = c("boldel",
                                           "bradel",
                                           "ecudel",
                                           "elsdel" ,
                                           "hondel",
                                           "mexdel",
                                           "pandel",
                                           "pardel",
                                           "perdel",
                                           "safdel",
                                           "argdel" ,
                                           "coldel",
                                           "domdel"),
                           code = c("BO","BR","EC",
                                    "SV","HO","MX",
                                    "PA","PY","PE",
                                    "ZA","AR","CO",
                                    "DO"),
                           audiencia_dev = "DEL")
  
  from_to = dplyr::bind_rows(from_to_DEL,from_to_CXC,from_to_BDR)
  
  return(from_to)
  
}


all_branches <- unique(APITalentMS(requisicao = "branches")$id)

user_info  <-  APITalentMS(requisicao = "users") %>%
  dplyr::select(
    user_first_name = first_name, user_last_name = last_name, email,
    user_id = id, user_login = login, user_created_on = created_on,
    main_user_type =  user_type, user_status = status,
    last_active = last_updated,
    user_points = points, contains("custom_field")) %>%
  dplyr::mutate(
    user_created_on = as.Date(substr(user_created_on,0,10),'%d/%m/%Y'),
    user_days_on_plataform = difftime(lubridate::today(), user_created_on,units = 'days') %>% as.integer()
  )

users_from_branches <- parallel_extract(all_branches, type = "branches", n_cluster = 1)

user_info_learners <- user_info %>% filter(!main_user_type %in% c('BEES-Academy-Focal-Point', 'SuperAdmin'))

users_complete <- left_join(user_info_learners, users_from_branches, by = "user_id")

users_extra_branch <- users_complete %>% group_by(user_id) %>% summarise(n = n()) %>% filter(n > 1) %>% pull(user_id)
#partnersp partners = ignorar
users_complete <- users_complete %>% filter(!branch_code %in% c("partnersp", "frontline", "partners", "samplebranch"))

# Criando depara region - country

depara_region_country <- users_complete %>%  
  filter(!is.na(branch_code)) %>% 
  filter(!is.na(custom_field_5)) %>% 
  filter(!custom_field_5 %in% c('NA','NA - GENERAL', 'CXC - NA')) %>% 
  group_by(branch_code, custom_field_5) %>% 
  summarise(n = n()) %>%
  ungroup() %>% 
  filter(n>1) %>% 
  select(branch_code,custom_field_5) %>% 
  dplyr::left_join(from_to_branch_name_to_code(), by = c("branch_code"="branch_name"), relationship = "many-to-many") %>%
  distinct(custom_field_5, .keep_all = T) %>% 
  select(custom_field_5, code) %>% 
  filter(custom_field_5 != 'METROPOLITANA - PY')

na_fix <- data.frame(custom_field_5 = c('NA - UK', 'METROPOLITANA - PY',
                                        'CD PORVENIR - HN', 'CORDOBA - AR',# aq
                                        'DC JUPILLE - SUR - MEUSE - BE', 'DC LA GOMERA - ESCAN',
                                        'DISTRIBUIDORES GBA NORTE (90) - AR', 'DISTRIBUIDORES GBA OESTE (91) - AR',
                                        'EC HIGH END COSTA - EC', 'EC HIGH END SIERRA - EC',
                                        'GBA ON - AR', 'KATO INVESTMENTS LTD - BULENGA - UG',
                                        'KKAA OFFCOSTA - EC', 'MAR DEL PLATA OFF - AR',
                                        'MAR DEL PLATA ON - AR', 'PE GER P3 PROVINCIAS - PE',
                                        'SIDONA AGENCY - MIGYERA&NAKASO - UG', 'SIDONA AGENCY  -  WOBULENZI - UG',
                                        'TAN DISTRIBUTORS LTD -  MOROTO - UG',
                                        "FB01 - MX","FC16 - MX","FD08 - MX","FK11 - MX",
                                        "FM02 - MX","FP08 - MX","FP14 - MX","FP23 - MX",
                                        "FS10 - MX", "FV04 - MX" ,"FX04 - MX"),
               code = c('UK', 'PY', 'HN', 'AR', 'BE', 'ESCAN', 'AR', 'AR', 'EC', 'EC',
                        'AR', 'UG', 'EC', 'AR', 'AR', 'PE', 'UG', 'UG', 'UG',
                        'MX','MX','MX','MX','MX','MX','MX','MX','MX','MX','MX'))

depara_region_country <- bind_rows(depara_region_country, na_fix) %>% 
  filter(!is.na(code)) %>% 
  mutate(AUDIENCIA_DEV = 'NA')

# FIM DO DE_PARA

# Adicionando a coluna code no users_complete

pt1 <- users_complete %>% #
  filter(!is.na(branch_code)) %>% 
  left_join(from_to_branch_name_to_code(), by = c("branch_code"="branch_name"), relationship = "many-to-many")

pt2 <-
  users_complete %>% 
  filter(is.na(branch_code)) %>% 
  left_join(depara_region_country, by = "custom_field_5", relationship = "many-to-many")

users_complete <- bind_rows(pt1,pt2)

# branches_interesse <- c("gloriaco", "loreal", "mercasidbdr", "nestleadnec", "nestlear", "nestlebr",
#                         "nestleindonesia", "nestlephilippines", "partners", "partnersp", "samplebranch")
country_interesse <- c("MERCASID", "NESTLEADNEC", "NESTLEPH", "NESTLEBR",
                       "GLORIACO", "LOREAL", "NESTLEAR", "NESTLEINDONESIA",
                       "PARTNERSBR", "NESTLEPH")

res <- users_complete %>% 
  filter(!branch_code %in% country_interesse) %>%
  mutate(custom_field_8 = toupper(custom_field_8)) %>% 
  group_by(code, custom_field_8, user_status) %>% 
  summarise(n = as.numeric(n())) %>%
  arrange(code) %>% 
  pivot_wider(names_from = custom_field_8, values_from = n) %>% 
  mutate_all(~ifelse(is.na(.), 0, .)) %>% 
  mutate(Soma = rowSums(across(where(is.numeric), as.numeric))) %>% 
  openxlsx::write.xlsx("users_count_abi_code.xlsx")


# res[-44,] %>% openxlsx::write.xlsx("users_count_abi.xlsx")
  
  
  
  